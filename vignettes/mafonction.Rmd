---
title: "mafonction"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{mafonction}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(examen)
```

<!-- WARNING - This vignette is generated by {fusen} from /dev/flat_minimal.Rmd: do not edit by hand -->

<!--
 You need to run the 'description' chunk in the '0-dev_history.Rmd' file before continuing your code there.
-->


# ma_fonction

    

  

```{r example-ma_fonction}
ma_fonction(data = iris,n_head = 3,n_tail = 3)
```

  

  


